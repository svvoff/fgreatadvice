//
//  NSString+FGAHTMLAttributedString.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "NSString+FGAHTMLAttributedString.h"
#import <UIKit/UIFont.h>
#import <UIKit/NSParagraphStyle.h>

@implementation NSString (FGAHTMLAttributedString)

- (NSAttributedString *) htmlAttributedString {
  NSData *textData = [self dataUsingEncoding:NSUTF8StringEncoding];
  NSDictionary *options = @{
                            NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                            NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)
                            };

  NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:textData
                                                                          options:options
                                                               documentAttributes:nil
                                                                            error:nil];
  NSMutableParagraphStyle *paragraphStyle = [NSParagraphStyle defaultParagraphStyle].mutableCopy;
  paragraphStyle.alignment = NSTextAlignmentCenter;
  NSDictionary *attributes = @{
                               NSFontAttributeName: [UIFont systemFontOfSize:17.f],
                               NSParagraphStyleAttributeName: paragraphStyle
                               };
  [attributedString setAttributes:attributes range:NSMakeRange(0, attributedString.length)];
  return attributedString;
}

@end
