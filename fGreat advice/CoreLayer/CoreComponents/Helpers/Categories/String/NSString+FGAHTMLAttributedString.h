//
//  NSString+FGAHTMLAttributedString.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/NSAttributedString.h>

@interface NSString (FGAHTMLAttributedString)

- (NSAttributedString *) htmlAttributedString;

@end
