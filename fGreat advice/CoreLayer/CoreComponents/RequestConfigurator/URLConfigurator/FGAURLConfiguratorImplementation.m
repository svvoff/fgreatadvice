//
//  FGAURLConfiguratorImplementation.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/15/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAURLConfiguratorImplementation.h"

static NSString *const kBaseUrlString = @"http://fucking-great-advice.ru/api";

@implementation FGAURLConfiguratorImplementation

- (NSURL *) urlTo:(NSString *)path {
  NSURL *url = [NSURL URLWithString:kBaseUrlString];
  url = [url URLByAppendingPathComponent:path];
  return url;
}

@end
