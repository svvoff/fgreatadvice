//
//  FGARESTRequestConfiguratorImplementation.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/15/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FGARESTRequestConfigurator.h"

@protocol FGAURLConfigurator;

@interface FGARESTRequestConfiguratorImplementation : NSObject <FGARESTRequestConfigurator>

- (instancetype) initWithURLConfigurator: (id<FGAURLConfigurator>) urlConfigurator;

@end
