//
//  FGARESTRequestConfiguratorImplementation.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/15/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGARESTRequestConfiguratorImplementation.h"
#import "FGAURLConfigurator.h"


@interface FGARESTRequestConfiguratorImplementation ()

@property (nonatomic, strong) id<FGAURLConfigurator> urlConfigurator;

@end

@implementation FGARESTRequestConfiguratorImplementation

- (instancetype) initWithURLConfigurator:(id<FGAURLConfigurator>)urlConfigurator {
  self = [super init];

  if (self) {
    _urlConfigurator = urlConfigurator;
  }

  return self;
}

- (NSURLRequest *) requestToPath: (NSString *) path {
  NSURL *url = [self.urlConfigurator urlTo:path];
  
  return [NSURLRequest requestWithURL:url];
}

@end
