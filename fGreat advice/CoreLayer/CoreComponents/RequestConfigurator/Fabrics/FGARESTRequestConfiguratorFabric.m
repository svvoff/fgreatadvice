//
//  FGARESTRequestConfiguratorFabric.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/15/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGARESTRequestConfiguratorFabric.h"
#import "FGAURLConfigurator.h"
#import "FGAURLConfiguratorFabric.h"
#import "FGARESTRequestConfiguratorImplementation.h"

@implementation FGARESTRequestConfiguratorFabric

- (id<FGARESTRequestConfigurator>) restRequestConfigurator {
  FGAURLConfiguratorFabric *urlConfiguratorFabric = [FGAURLConfiguratorFabric new];
  id<FGAURLConfigurator> urlConfigurator = [urlConfiguratorFabric urlConfigurator];

  FGARESTRequestConfiguratorImplementation *restRequestConfigurator = [[FGARESTRequestConfiguratorImplementation alloc]
                                                                       initWithURLConfigurator:urlConfigurator];
  return restRequestConfigurator;
}

@end
