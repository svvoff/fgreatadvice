//
//  FGAURLConfiguratorFabric.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/15/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FGAURLConfigurator.h"

@interface FGAURLConfiguratorFabric : NSObject

- (id<FGAURLConfigurator>) urlConfigurator;

@end
