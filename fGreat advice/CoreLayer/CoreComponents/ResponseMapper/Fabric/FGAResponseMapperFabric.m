//
//  FGAResponseMapperFabric.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAResponseMapperFabric.h"
#import "FGAAdviceResponseMapper.h"

@implementation FGAResponseMapperFabric

- (id<FGAResponseMapper>)adviceResponseMapper {
  return [FGAAdviceResponseMapper new];
}

@end
