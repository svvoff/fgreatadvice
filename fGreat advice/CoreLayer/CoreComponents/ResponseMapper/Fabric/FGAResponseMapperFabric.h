//
//  FGAResponseMapperFabric.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FGAResponseMapper;

@interface FGAResponseMapperFabric : NSObject

- (id<FGAResponseMapper>) adviceResponseMapper;

@end
