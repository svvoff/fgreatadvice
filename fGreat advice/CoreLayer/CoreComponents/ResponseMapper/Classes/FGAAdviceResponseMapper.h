//
//  FGAAdviceResponseMapper.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FGAResponseMapper.h"

@interface FGAAdviceResponseMapper : NSObject <FGAResponseMapper>

@end
