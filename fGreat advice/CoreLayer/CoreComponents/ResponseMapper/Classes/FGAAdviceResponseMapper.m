//
//  FGAAdviceResponseMapper.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAAdviceResponseMapper.h"
#import "FGAAdvice.h"


@implementation FGAAdviceResponseMapper

- (id) mapFrom:(NSDictionary *)json {
  FGAAdvice *advice = [FGAAdvice new];
  advice.idNumber = json[@"id"];
  advice.text = json[@"text"];
  return advice;
}

@end
