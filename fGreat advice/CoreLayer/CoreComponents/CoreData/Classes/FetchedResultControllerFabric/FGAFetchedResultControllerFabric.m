//
//  FGAFetchedResultControllerFabric.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAFetchedResultControllerFabric.h"
#import "FGACoreDataStack.h"
#import "Advice+CoreDataProperties.h"

@implementation FGAFetchedResultControllerFabric

- (NSFetchedResultsController *) favouritesFetchedResultController {
  NSManagedObjectContext *context = [FGACoreDataStack defauldManagedContext];
  NSFetchRequest *adviceFetchedRequest = [Advice fetchRequest];
  NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"idNumber" ascending:YES];
  adviceFetchedRequest.sortDescriptors = @[sortDescriptor];
  NSFetchedResultsController *fetchedResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:adviceFetchedRequest
                                                                                            managedObjectContext:context
                                                                                              sectionNameKeyPath:nil
                                                                                                       cacheName:nil];

  return fetchedResultController;
}

@end
