//
//  FGAFetchedResultControllerFabric.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/NSFetchedResultsController.h>

@interface FGAFetchedResultControllerFabric : NSObject

- (NSFetchedResultsController *) favouritesFetchedResultController;

@end
