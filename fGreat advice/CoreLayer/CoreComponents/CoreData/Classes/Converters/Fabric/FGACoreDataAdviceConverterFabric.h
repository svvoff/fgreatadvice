//
//  FGACoreDataAdviceConverterFabric.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/17/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol FGACoreDataAdviceConverter;

@interface FGACoreDataAdviceConverterFabric : NSObject

- (id<FGACoreDataAdviceConverter>) coreDataAdviceConverter;

@end
