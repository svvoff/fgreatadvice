//
//  FGACoreDataAdviceConverterFabric.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/17/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGACoreDataAdviceConverterFabric.h"
#import "FGACoreDataAdviceConverterImplementation.h"

@implementation FGACoreDataAdviceConverterFabric

- (id<FGACoreDataAdviceConverter>) coreDataAdviceConverter {
  return [FGACoreDataAdviceConverterImplementation new];
}

@end
