//
//  FGACoreDataAdviceConverterImplementation.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGACoreDataAdviceConverterImplementation.h"
#import "Advice+CoreDataProperties.h"
#import "FGAAdvice.h"
#import "NSString+FGAHTMLAttributedString.h"
#import <CoreData/NSManagedObject.h>

@implementation FGACoreDataAdviceConverterImplementation

- (FGAAdvice *) convertManaged:(Advice *)advice {
  FGAAdvice *modelAdvice = [FGAAdvice new];
  modelAdvice.idNumber = @(advice.idNumber);
  modelAdvice.text = advice.text;
  return modelAdvice;
}

- (Advice *) convertModel:(FGAAdvice *)advice inContext:(NSManagedObjectContext *)context {
  Advice *managedAdvice = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Advice class])
                                                        inManagedObjectContext:context];
  managedAdvice.idNumber = advice.idNumber.integerValue;
  managedAdvice.text = advice.text;
  return managedAdvice;
}

@end
