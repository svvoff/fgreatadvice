//
//  FGACoreDataAdviceConverter.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FGAAdvice, Advice, NSManagedObjectContext;

@protocol FGACoreDataAdviceConverter <NSObject>

- (FGAAdvice *) convertManaged: (Advice *) advice;
- (Advice *) convertModel: (FGAAdvice *) advice inContext: (NSManagedObjectContext *) context;

@end
