//
//  FGACoreDataStack.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGACoreDataStack.h"


static NSPersistentContainer *persistentContainer_ = nil;

static NSString *const kCoreDataModelName = @"FGAModel";

@interface FGACoreDataStack ()

@end

@implementation FGACoreDataStack

#pragma mark - Class

+ (NSManagedObjectContext *) defauldManagedContext {
  return persistentContainer_.viewContext;
}

+ (void) initialize {
  if (self == [FGACoreDataStack class]) {
    persistentContainer_ = [[NSPersistentContainer alloc] initWithName:kCoreDataModelName];
    [persistentContainer_ loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *description, NSError *error) {
      if (error != nil) {
        NSLog(@"Failed to load Core Data stack: %@", error);
        abort();
      }
    }];
  }
}

@end
