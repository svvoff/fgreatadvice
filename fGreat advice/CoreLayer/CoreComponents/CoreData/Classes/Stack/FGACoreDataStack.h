//
//  FGACoreDataStack.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface FGACoreDataStack : NSObject

+ (NSManagedObjectContext *) defauldManagedContext;

@end
