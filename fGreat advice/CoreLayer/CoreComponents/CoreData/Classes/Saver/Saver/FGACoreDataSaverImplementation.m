//
//  FGACoreDataSaverImplementation.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGACoreDataSaverImplementation.h"
#import <CoreData/CoreData.h>

@implementation FGACoreDataSaverImplementation

- (void) saveChangesInManagedObjectContext:(NSManagedObjectContext *)context
                             synchronously:(BOOL)synchronously
                                completion:(FGACoreDataSaverCompletion)block {

  __block BOOL hasChanges = NO;
  [context performBlockAndWait:^{
    hasChanges = [context hasChanges];
  }];

  if (!hasChanges && block) {
    block();
    return;
  }

  id saveBlock = ^{
    BOOL done = NO;
    NSError *error = nil;

    @try {
      done = [context save:&error];
    } @catch (NSException *exception) {
      NSLog(@"Can not to save %@", exception.userInfo ?: exception.reason);
    } @finally {
      if (done && context.parentContext) {
        [self saveChangesInManagedObjectContext:context.parentContext synchronously:synchronously completion:block];
      } else if (block) {
        block();
      }
    }
  };

  if (synchronously) {
    [context performBlockAndWait:saveBlock];
  } else {
    [context performBlock:saveBlock];
  }
}

@end
