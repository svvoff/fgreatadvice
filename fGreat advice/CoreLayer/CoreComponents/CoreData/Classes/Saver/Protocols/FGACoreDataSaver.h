//
//  FGACoreDataSaver.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NSManagedObjectContext;

typedef void(^FGACoreDataSaverCompletion)(void);

@protocol FGACoreDataSaver <NSObject>

- (void) saveChangesInManagedObjectContext: (NSManagedObjectContext *) context
                             synchronously: (BOOL) synchronously
                                completion: (FGACoreDataSaverCompletion) block;

@end
