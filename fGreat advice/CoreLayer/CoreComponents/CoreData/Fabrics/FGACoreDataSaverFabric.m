//
//  FGACoreDataSaverFabric.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGACoreDataSaverFabric.h"
#import "FGACoreDataSaverImplementation.h"

@implementation FGACoreDataSaverFabric

- (id<FGACoreDataSaver>) coreDataSaver {
  FGACoreDataSaverImplementation *coreDataSaver = [FGACoreDataSaverImplementation new];
  return coreDataSaver;
}

@end
