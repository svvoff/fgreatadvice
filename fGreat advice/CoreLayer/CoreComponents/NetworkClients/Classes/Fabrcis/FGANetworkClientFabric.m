//
//  FGANetworkClientFabric.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/15/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGANetworkClientFabric.h"
#import "FGACommonNetworkClient.h"

@implementation FGANetworkClientFabric

- (id<FGANetworkClient>) commonNetworkClient {

  NSURLSession *session = [NSURLSession sharedSession];
  FGACommonNetworkClient *client = [[FGACommonNetworkClient alloc] initWithURLSession:session];

  return client;
}

@end
