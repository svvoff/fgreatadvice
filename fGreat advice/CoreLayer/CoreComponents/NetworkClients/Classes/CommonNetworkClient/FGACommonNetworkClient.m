//
//  FGACommonNetworkClient.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/15/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGACommonNetworkClient.h"

NSErrorDomain const FGANetworkErrorDomain = @"FGANetworkErrorDomain";

@interface FGACommonNetworkClient ()

@property (nonatomic, strong) NSURLSession *session;

@end

@implementation FGACommonNetworkClient

- (instancetype) initWithURLSession:(NSURLSession *)session {
  self = [super init];

  if (self) {
    _session = session;
  }

  return self;
}

- (void) sendRequest:(NSURLRequest *)urlRequest completion:(FGANetworkClientCompletion)block {
  NSURLSessionDataTask *task = [self.session dataTaskWithRequest:urlRequest
                                               completionHandler:^(NSData * _Nullable data,
                                                                   NSURLResponse * _Nullable response,
                                                                   NSError * _Nullable error) {
                                                 if (!block) {
                                                   return;
                                                 }

                                                 if (!data && !error) {
                                                   NSError *emptyDataError = [NSError errorWithDomain:FGANetworkErrorDomain code:FGANetworkEmptyDataError userInfo:nil];
                                                   block(nil, emptyDataError);
                                                   return;
                                                 }

                                                 block(data, error);
                                               }];
  
  [task resume];
}

@end
