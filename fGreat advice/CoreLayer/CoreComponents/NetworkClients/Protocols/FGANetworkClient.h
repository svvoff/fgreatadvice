//
//  FGANetworkClient.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/15/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSErrorDomain const FGANetworkErrorDomain;

typedef NS_ENUM(NSInteger, FGANetworkError) {
  FGANetworkEmptyDataError = 1000
};

typedef void(^FGANetworkClientCompletion)(NSData *data, NSError *error);

@protocol FGANetworkClient <NSObject>

- (void) sendRequest:(NSURLRequest *) urlRequest completion: (FGANetworkClientCompletion) block;

@end
