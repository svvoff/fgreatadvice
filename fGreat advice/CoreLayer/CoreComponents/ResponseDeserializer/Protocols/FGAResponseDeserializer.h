//
//  FGAResponseDeserializer.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FGAResponseDeserializer <NSObject>

- (NSDictionary *) jsonDictionaryFromData:(NSData *) data error:(NSError **) error;

@end
