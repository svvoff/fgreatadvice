//
//  FGAResponseDeserializerFabric.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAResponseDeserializerFabric.h"
#import "FGAResponseDeserializerImplementation.h"

@implementation FGAResponseDeserializerFabric

- (id<FGAResponseDeserializer>) responseDeserializer {
  FGAResponseDeserializerImplementation *responseDeserializer = [FGAResponseDeserializerImplementation new];

  return responseDeserializer;
}

@end
