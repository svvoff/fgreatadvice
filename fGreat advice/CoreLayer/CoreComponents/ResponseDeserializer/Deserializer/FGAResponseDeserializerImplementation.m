//
//  FGAResponseDeserializer.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAResponseDeserializerImplementation.h"


@implementation FGAResponseDeserializerImplementation

- (NSDictionary *) jsonDictionaryFromData:(nonnull NSData *)data error:(NSError **)error {

  NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:error];

  return json;
}

@end
