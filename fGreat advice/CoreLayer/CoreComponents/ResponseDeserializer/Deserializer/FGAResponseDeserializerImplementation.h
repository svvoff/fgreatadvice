//
//  FGAResponseDeserializer.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FGAResponseDeserializer.h"

@interface FGAResponseDeserializerImplementation : NSObject <FGAResponseDeserializer>

@end
