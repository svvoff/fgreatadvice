//
//  FGAFavouriteAdvicesControlService.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAFavouriteAdvicesControlServiceImplementation.h"
#import "Advice+CoreDataProperties.h"
#import "FGACoreDataStack.h"
#import "FGAAdvice.h"


@implementation FGAFavouriteAdvicesControlServiceImplementation

- (void) saveAdvice: (FGAAdvice *) advice {
  NSManagedObjectContext *defaultContext = [FGACoreDataStack defauldManagedContext];
  NSFetchRequest *adviceFetchRequest = [self adviceFetchRequestForIdNumber:advice.idNumber];
  NSError *error = nil;
  if ([defaultContext executeFetchRequest:adviceFetchRequest error:&error].count == 0) {
    [self.adviceConverter convertModel:advice inContext:defaultContext];
    [self.saver saveChangesInManagedObjectContext:defaultContext synchronously:NO completion:nil];
  }
}

- (void) removeAdviceById: (NSNumber *) adviceId {
  NSManagedObjectContext *context = [FGACoreDataStack defauldManagedContext];
  NSFetchRequest *adviceFetchedRequest = [self adviceFetchRequestForIdNumber:adviceId];
  NSError *error = nil;
  NSArray *advices = [context executeFetchRequest:adviceFetchedRequest error:&error];
  Advice *removingAdvice = [advices firstObject];
  if (removingAdvice) {
    [context deleteObject:removingAdvice];
    [self.saver saveChangesInManagedObjectContext:context synchronously:YES completion:nil];
  }
}

- (NSFetchRequest *) adviceFetchRequestForIdNumber: (NSNumber *) idNumber {
  NSFetchRequest *adviceFetchRequest = [Advice fetchRequest];
  adviceFetchRequest.fetchLimit = 1;
  adviceFetchRequest.predicate = [NSPredicate predicateWithFormat:@"idNumber == %@", idNumber];
  return adviceFetchRequest;
}

@end
