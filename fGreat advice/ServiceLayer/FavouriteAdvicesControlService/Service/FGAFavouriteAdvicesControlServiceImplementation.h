//
//  FGAFavouriteAdvicesControlService.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FGAFavouriteAdvicesControlService.h"
#import "FGACoreDataSaver.h"
#import "FGACoreDataAdviceConverter.h"

@interface FGAFavouriteAdvicesControlServiceImplementation : NSObject <FGAFavouriteAdvicesControlService>

@property (nonatomic, strong) id<FGACoreDataSaver> saver;
@property (nonatomic, strong) id<FGACoreDataAdviceConverter> adviceConverter;

@end
