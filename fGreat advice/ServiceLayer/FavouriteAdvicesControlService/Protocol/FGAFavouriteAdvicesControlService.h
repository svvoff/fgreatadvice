//
//  FGAFavouriteAdvicesControlService.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FGAAdvice;

@protocol FGAFavouriteAdvicesControlService <NSObject>

- (void) saveAdvice: (FGAAdvice *) advice;
- (void) removeAdviceById: (NSNumber *) idNumber;

@end
