//
//  FGAFavouriteAdvicesControlServiceFabric.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAFavouriteAdvicesControlServiceFabric.h"
#import "FGAFavouriteAdvicesControlServiceImplementation.h"
#import "FGACoreDataSaverFabric.h"
#import "FGACoreDataAdviceConverterFabric.h"


@implementation FGAFavouriteAdvicesControlServiceFabric

- (id<FGAFavouriteAdvicesControlService>) favoriteAdvicesControlService {
  FGAFavouriteAdvicesControlServiceImplementation *favouriteAdvicesControlService =  [FGAFavouriteAdvicesControlServiceImplementation new];

  FGACoreDataSaverFabric *coreDataSaverFabric = [FGACoreDataSaverFabric new];
  id<FGACoreDataSaver> coreDataSaver = [coreDataSaverFabric coreDataSaver];
  favouriteAdvicesControlService.saver = coreDataSaver;

  FGACoreDataAdviceConverterFabric *adviceConverterFabric = [FGACoreDataAdviceConverterFabric new];
  id<FGACoreDataAdviceConverter> adviceConverter = [adviceConverterFabric coreDataAdviceConverter];
  favouriteAdvicesControlService.adviceConverter = adviceConverter;

  return favouriteAdvicesControlService;
}

@end
