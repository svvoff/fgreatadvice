//
//  FGAFavouriteAdvicesControlServiceFabric.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol FGAFavouriteAdvicesControlService;

@interface FGAFavouriteAdvicesControlServiceFabric : NSObject

- (id<FGAFavouriteAdvicesControlService>) favoriteAdvicesControlService;

@end
