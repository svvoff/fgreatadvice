//
//  FGAAdviceLoadSchedulerServiceDelegate.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/17/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FGAAdviceLoadSchedulerServiceDelegate <NSObject>

- (void) timeToLoadAdvice;

@end
