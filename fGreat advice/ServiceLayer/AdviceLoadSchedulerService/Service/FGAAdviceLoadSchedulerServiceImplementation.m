//
//  FGAAdviceLoadSchedulerServiceImplementation.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/17/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAAdviceLoadSchedulerServiceImplementation.h"
#import "FGAAdviceLoadSchedulerServiceDelegate.h"


static NSTimeInterval const kAdviceLoadPeriod = 30.f;

@interface FGAAdviceLoadSchedulerServiceImplementation ()

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation FGAAdviceLoadSchedulerServiceImplementation

@synthesize delegate;


- (void) start {
  [self configureTimer];
}

- (void) reset {
  [self configureTimer];
}

- (void) onTick {
  [self.delegate timeToLoadAdvice];
}

- (void) configureTimer {
  [self.timer invalidate];
  dispatch_async(dispatch_get_main_queue(), ^{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:kAdviceLoadPeriod
                                                  target:self
                                                selector:@selector(onTick)
                                                userInfo:nil
                                                 repeats:YES];
  });
}

@end
