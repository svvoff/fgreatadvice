//
//  FGAAdviceLoadSchedulerServiceFabric.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/17/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAAdviceLoadSchedulerServiceFabric.h"
#import "FGAAdviceLoadSchedulerServiceImplementation.h"

@implementation FGAAdviceLoadSchedulerServiceFabric

- (id<FGAAdviceLoadSchedulerService>) adviceLoadSchedulerService {
  FGAAdviceLoadSchedulerServiceImplementation *adviceLoadSchedulerService = [FGAAdviceLoadSchedulerServiceImplementation new];

  return adviceLoadSchedulerService;
}

@end
