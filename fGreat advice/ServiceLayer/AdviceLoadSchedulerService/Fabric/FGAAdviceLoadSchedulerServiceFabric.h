//
//  FGAAdviceLoadSchedulerServiceFabric.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/17/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol FGAAdviceLoadSchedulerService;

@interface FGAAdviceLoadSchedulerServiceFabric : NSObject

- (id<FGAAdviceLoadSchedulerService>) adviceLoadSchedulerService;

@end
