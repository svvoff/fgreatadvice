//
//  FGAAdviceLoadServiceFabric.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/15/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FGAAdviceLoadService.h"

@interface FGAAdviceLoadServiceFabric : NSObject

- (id<FGAAdviceLoadService>) adviceLoadService;

@end
