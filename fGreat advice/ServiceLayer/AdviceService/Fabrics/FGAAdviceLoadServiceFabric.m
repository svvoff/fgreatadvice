//
//  FGAAdviceLoadServiceFabric.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/15/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAAdviceLoadServiceFabric.h"
#import "FGANetworkClientFabric.h"
#import "FGANetworkClient.h"
#import "FGARESTRequestConfiguratorFabric.h"
#import "FGARESTRequestConfigurator.h"
#import "FGAAdviceLoadServiceImplementation.h"
#import "FGAResponseDeserializerFabric.h"
#import "FGAResponseMapperFabric.h"

@implementation FGAAdviceLoadServiceFabric

- (id<FGAAdviceLoadService>) adviceLoadService {
  FGANetworkClientFabric *networkClientFabric = [FGANetworkClientFabric new];
  id<FGANetworkClient> networkClient = [networkClientFabric commonNetworkClient];

  FGARESTRequestConfiguratorFabric *restRequestConfiguratorFabric = [FGARESTRequestConfiguratorFabric new];
  id<FGARESTRequestConfigurator> requestConfigurator = [restRequestConfiguratorFabric restRequestConfigurator];

  FGAResponseDeserializerFabric *responseDeserializerFabric = [FGAResponseDeserializerFabric new];
  id<FGAResponseDeserializer> responseDeserializer = [responseDeserializerFabric responseDeserializer];

  FGAResponseMapperFabric *responseMapperFabric = [FGAResponseMapperFabric new];
  id<FGAResponseMapper> responseMapper = [responseMapperFabric adviceResponseMapper];

  FGAAdviceLoadServiceImplementation *adviceLoadService = [[FGAAdviceLoadServiceImplementation alloc] initWithNetworkClient:networkClient
                                                                                                        requestConfigurator:requestConfigurator
                                                                                                       responseDeserializer:responseDeserializer
                                                                                                             responseMapper:responseMapper];
  return adviceLoadService;
}

@end
