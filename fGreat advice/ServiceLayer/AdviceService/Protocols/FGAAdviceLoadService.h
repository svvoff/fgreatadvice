//
//  FGAAdviceLoadService.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/15/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FGAAdvice;

typedef void(^FGAAdviceLoadServiceCompletion)(FGAAdvice *advice, NSError *error);

@protocol FGAAdviceLoadService <NSObject>

- (void) loadNewAdviceWithCompletion: (FGAAdviceLoadServiceCompletion) block;

@end
