//
//  FGAAdviceLoadServiceImplementation.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/15/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAAdviceLoadServiceImplementation.h"
#import "FGAAdvice.h"

static NSString *const kAdvicePath = @"random";

@interface FGAAdviceLoadServiceImplementation ()

@property (nonatomic, strong) id<FGANetworkClient> networkClient;
@property (nonatomic, strong) id<FGARESTRequestConfigurator> restRequestConfigurator;
@property (nonatomic, strong) id<FGAResponseDeserializer> responseDeserializer;
@property (nonatomic, strong) id<FGAResponseMapper> responseMapper;

@end

@implementation FGAAdviceLoadServiceImplementation

- (instancetype) initWithNetworkClient:(id<FGANetworkClient>)networkClient
                   requestConfigurator:(id<FGARESTRequestConfigurator>)requestConfigurator
                  responseDeserializer:(id<FGAResponseDeserializer>) responseDeserializer
                        responseMapper:(id<FGAResponseMapper>) responseMapper {

  self = [super init];

  if (self) {
    _networkClient = networkClient;
    _restRequestConfigurator = requestConfigurator;
    _responseDeserializer = responseDeserializer;
    _responseMapper = responseMapper;
  }

  return self;
}

- (void) loadNewAdviceWithCompletion:(FGAAdviceLoadServiceCompletion)block {
  NSURLRequest *adviceRequest = [self.restRequestConfigurator requestToPath:kAdvicePath];
  [self.networkClient sendRequest:adviceRequest completion:^(NSData *data, NSError *error) {

    if (!block) {
      return;
    }

    if (error) {
      block(nil, error);
      return;
    }

    NSError *jsonSerializationError;
    NSDictionary *json = [self.responseDeserializer jsonDictionaryFromData:data error:&jsonSerializationError];

    FGAAdvice *advice = [self.responseMapper mapFrom:json];

    if (block) {
      block(advice, jsonSerializationError);
    }
  }];
}

@end
