//
//  FGAAdviceLoadServiceImplementation.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/15/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FGAAdviceLoadService.h"
#import "FGARESTRequestConfigurator.h"
#import "FGANetworkClient.h"
#import "FGAResponseDeserializer.h"
#import "FGAResponseMapper.h"

@interface FGAAdviceLoadServiceImplementation : NSObject <FGAAdviceLoadService>

- (instancetype) initWithNetworkClient:(id<FGANetworkClient>) networkClient
                   requestConfigurator:(id<FGARESTRequestConfigurator>) requestConfigurator
                  responseDeserializer:(id<FGAResponseDeserializer>) responseDeserializer
                        responseMapper:(id<FGAResponseMapper>) responseMapper;


@end
