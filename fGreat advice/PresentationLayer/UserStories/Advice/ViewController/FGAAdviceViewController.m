//
//  AdviceViewController.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/14/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAAdviceViewController.h"
#import "FGASegmentedViewControllerContainerChild.h"
#import "FGASegmentedViewControllerContainer.h"
#import "FGAAdviceLoadService.h"
#import "FGAAdvice.h"
#import "FGAFavouriteAdvicesControlService.h"
#import "FGAAdviceLoadSchedulerService.h"
#import "NSString+FGAHTMLAttributedString.h"

@interface FGAAdviceViewController () <FGASegmentedViewControllerContainerChild>

@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, strong) UIBarButtonItem *rightBarButton;
@property (nonatomic, strong) UIBarButtonItem *leftBarButton;
@property (nonatomic, strong) UIActivityIndicatorView *indicator;
@property (nonatomic, assign) BOOL waitingForAdvice;
@property (nonatomic, strong) FGAAdvice *currentAdvice;

@end

@implementation FGAAdviceViewController

@synthesize segmentedContainer = _segmentedContainer;

- (void)viewDidLoad {
  [super viewDidLoad];
  self.waitingForAdvice = NO;
  self.rightBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                      target:self
                                                                      action:@selector(refreshButtonTaped:)];
  self.leftBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self
                                                                     action:@selector(saveButtonTaped:)];
  self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
  [self.indicator startAnimating];
  [self loadAdvice];
  [self.adviceLoadSchedulerService start];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}


#pragma mark - Actions

- (void) refreshButtonTaped:(UIBarButtonItem *) button {
  [self loadAdvice];
  [self.adviceLoadSchedulerService reset];
}

- (void) saveButtonTaped:(UIBarButtonItem *) button {
  if (self.currentAdvice) {
    [self.favouriteAdvicesControlService saveAdvice:self.currentAdvice];
  }
}


#pragma mark - Navitem configuration

- (void) configureNavigationItem:(UINavigationItem *) navigationItem waitingAdvice:(BOOL) waitingAdvice {
  navigationItem.rightBarButtonItem = waitingAdvice ? [[UIBarButtonItem alloc] initWithCustomView:self.indicator] : self.rightBarButton;
  navigationItem.leftBarButtonItem = self.leftBarButton;
}


#pragma mark - FGASegmentedViewControllerContainerChild

- (void) configureSegmentedViewControllerContainerNavigationItem:(UINavigationItem *)navigationItem {
  [self configureNavigationItem:navigationItem waitingAdvice:self.waitingForAdvice];
}


#pragma mark - Error

- (void) showErrorAlert {
  UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Warning!"
                                                                 message:@"Something has gone wrong."
                                                          preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction *okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
  [alert addAction:okayAction];
  [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - Logic

- (void) loadAdvice {
  self.waitingForAdvice = YES;
  [self configureSegmentedViewControllerContainerNavigationItem:self.segmentedContainer.navigationItem];
  [self.adviceLoadService loadNewAdviceWithCompletion:^(FGAAdvice *advice, NSError *error) {
    dispatch_async(dispatch_get_main_queue(), ^{
      if (error) {
        [self showErrorAlert];
      } else {
        self.currentAdvice = advice;
        self.textView.attributedText = [advice.text htmlAttributedString];
      }
      self.waitingForAdvice = NO;
      [self configureSegmentedViewControllerContainerNavigationItem:self.segmentedContainer.navigationItem];
    });
  }];
}


#pragma mark - FGAAdviceLoadSchedulerServiceDelegate

- (void) timeToLoadAdvice {
  [self loadAdvice];
}

@end
