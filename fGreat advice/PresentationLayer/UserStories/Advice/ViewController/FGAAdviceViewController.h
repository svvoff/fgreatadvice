//
//  AdviceViewController.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/14/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FGAAdviceLoadSchedulerServiceDelegate.h"

@protocol FGAAdviceLoadService;
@protocol FGAFavouriteAdvicesControlService;
@protocol FGAAdviceLoadSchedulerService;

@interface FGAAdviceViewController : UIViewController <FGAAdviceLoadSchedulerServiceDelegate>

@property (nonatomic, strong) id<FGAAdviceLoadService> adviceLoadService;
@property (nonatomic, strong) id<FGAFavouriteAdvicesControlService> favouriteAdvicesControlService;
@property (nonatomic, strong) id<FGAAdviceLoadSchedulerService> adviceLoadSchedulerService;

@end
