//
//  FGAAdviceViewControllerConfigurator.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/15/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAAdviceViewControllerConfigurator.h"
#import "FGAAdviceLoadServiceFabric.h"
#import "FGAFavouriteAdvicesControlServiceFabric.h"
#import "FGAAdviceLoadSchedulerServiceFabric.h"
#import "FGAAdviceLoadSchedulerService.h"

@implementation FGAAdviceViewControllerConfigurator

- (void) awakeFromNib {
  [super awakeFromNib];

  FGAAdviceLoadServiceFabric *adviceLoadServiceFabric = [FGAAdviceLoadServiceFabric new];
  id<FGAAdviceLoadService> adviceLoadService = [adviceLoadServiceFabric adviceLoadService];

  self.adviceViewController.adviceLoadService = adviceLoadService;

  FGAFavouriteAdvicesControlServiceFabric *favouriteAdvicesControlServiceFabric = [FGAFavouriteAdvicesControlServiceFabric new];
  id<FGAFavouriteAdvicesControlService> favouriteAdvicesControlService = [favouriteAdvicesControlServiceFabric favoriteAdvicesControlService];

  self.adviceViewController.favouriteAdvicesControlService = favouriteAdvicesControlService;

  FGAAdviceLoadSchedulerServiceFabric *adviceLoadSchedulerFabric = [FGAAdviceLoadSchedulerServiceFabric new];
  id<FGAAdviceLoadSchedulerService> adviceLoadSchedulerService = [adviceLoadSchedulerFabric adviceLoadSchedulerService];
  self.adviceViewController.adviceLoadSchedulerService = adviceLoadSchedulerService;
  adviceLoadSchedulerService.delegate = self.adviceViewController;
}

@end
