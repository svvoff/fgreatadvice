//
//  FavouritesTableViewController.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/14/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAFavouritesTableViewController.h"
#import "Advice+CoreDataProperties.h"
#import "FGAFavouriteAdvicesControlService.h"
#import "FGAFavouritesTableViewCellConfigurator.h"

static NSString *kFavouriteTableViewCellIdentifier = @"kFavouriteTableViewCellIdentifier";

@implementation FGAFavouritesTableViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.tableView.rowHeight = UITableViewAutomaticDimension;
  self.tableView.estimatedRowHeight = 44.f;
  NSError *error = nil;
  [self.fetchedResultController performFetch:&error];
  if (error) {
    abort();
  }
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return [[self.fetchedResultController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  if (self.fetchedResultController.sections.count) {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultController.sections objectAtIndex:section];
    NSUInteger rows = sectionInfo.numberOfObjects;
    return rows;
  }
  return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kFavouriteTableViewCellIdentifier forIndexPath:indexPath];

  Advice *advice = [self.fetchedResultController objectAtIndexPath:indexPath];
  [self.cellConfigurator configureCell:cell withAdvice:advice];

  return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
  return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
  if (editingStyle == UITableViewCellEditingStyleDelete) {
    Advice *removingAdvice = [self.fetchedResultController objectAtIndexPath:indexPath];
    NSNumber *removingIdNumber = @(removingAdvice.idNumber);
    [self.favouriteAdvicesControlService removeAdviceById:removingIdNumber];
  }
}


//#pragma mark - TableViewDelegate
//
//- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//  return UITableViewAutomaticDimension;
//}


#pragma mark - NSFetchedResultsControllerDelegate

- (void) controllerWillChangeContent:(NSFetchedResultsController *)controller {
  [self.tableView beginUpdates];
}

- (void) controllerDidChangeContent:(NSFetchedResultsController *)controller {
  [self.tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(nullable NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(nullable NSIndexPath *)newIndexPath {
  switch (type) {
    case NSFetchedResultsChangeDelete:
      [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
      break;
    case NSFetchedResultsChangeInsert:
      [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
      break;
    default:
      break;
  }
}


@end
