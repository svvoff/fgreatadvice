//
//  FavouritesTableViewController.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/14/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/NSFetchedResultsController.h>

@protocol FGAFavouriteAdvicesControlService;
@protocol FGAFavouritesTableViewCellConfigurator;

@interface FGAFavouritesTableViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultController;
@property (nonatomic, strong) id<FGAFavouriteAdvicesControlService> favouriteAdvicesControlService;
@property (nonatomic, strong) id<FGAFavouritesTableViewCellConfigurator> cellConfigurator;

@end
