//
//  FGAFavouritesTableViewCellConfiguratorFabric.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/17/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAFavouritesTableViewCellConfiguratorFabric.h"
#import "FGAFavouritesTableViewCellConfiguratorImplementation.h"
#import "FGACoreDataAdviceConverterFabric.h"

@implementation FGAFavouritesTableViewCellConfiguratorFabric

- (id<FGAFavouritesTableViewCellConfigurator>) cellConfigurator {
  FGACoreDataAdviceConverterFabric *coreDataAdviceConverterFabric = [FGACoreDataAdviceConverterFabric new];
  id<FGACoreDataAdviceConverter> coreDataAdviceConverter = [coreDataAdviceConverterFabric coreDataAdviceConverter];
  FGAFavouritesTableViewCellConfiguratorImplementation *favouritesTableViewCellConfigurator = [[FGAFavouritesTableViewCellConfiguratorImplementation alloc] initWith:coreDataAdviceConverter];
  return favouritesTableViewCellConfigurator;
}

@end
