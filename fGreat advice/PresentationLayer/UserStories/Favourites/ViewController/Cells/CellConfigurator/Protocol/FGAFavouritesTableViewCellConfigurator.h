//
//  FGAFavouritesTableViewCellConfigurator.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/17/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UITableViewCell.h>
#import "Advice+CoreDataProperties.h"

@protocol FGAFavouritesTableViewCellConfigurator <NSObject>

- (void) configureCell:(UITableViewCell *) cell withAdvice:(Advice *) advice;

@end
