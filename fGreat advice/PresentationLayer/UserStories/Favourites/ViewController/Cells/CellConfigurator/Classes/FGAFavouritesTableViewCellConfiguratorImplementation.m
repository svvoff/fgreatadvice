//
//  FGAFavouritesTableViewCellConfiguratorImplementation.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/17/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAFavouritesTableViewCellConfiguratorImplementation.h"
#import "FGACoreDataAdviceConverter.h"
#import "FGAAdvice.h"
#import <UIKit/UILabel.h>
#import "NSString+FGAHTMLAttributedString.h"


@interface FGAFavouritesTableViewCellConfiguratorImplementation ()

@property (nonatomic, strong) id<FGACoreDataAdviceConverter> adviceConverter;

@end

@implementation FGAFavouritesTableViewCellConfiguratorImplementation

- (instancetype) initWith:(id<FGACoreDataAdviceConverter>)adviceConverter {
  self = [super init];

  if (self) {
    _adviceConverter = adviceConverter;
  }

  return self;
}

- (void) configureCell:(UITableViewCell *) cell withAdvice:(Advice *) advice {
  FGAAdvice *modelAdvice = [self.adviceConverter convertManaged:advice];
  cell.textLabel.attributedText = [modelAdvice.text htmlAttributedString];
}

@end
