//
//  FGAFavouritesTableViewCellConfiguratorImplementation.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/17/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FGAFavouritesTableViewCellConfigurator.h"

@protocol FGACoreDataAdviceConverter;

@interface FGAFavouritesTableViewCellConfiguratorImplementation : NSObject <FGAFavouritesTableViewCellConfigurator>

- (instancetype) initWith:(id<FGACoreDataAdviceConverter>) adviceConverter;

@end
