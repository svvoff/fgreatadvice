//
//  FGAFavouritesTableViewControllerConfigurator.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGAFavouritesTableViewControllerConfigurator.h"
#import "FGAFetchedResultControllerFabric.h"
#import "FGAFavouriteAdvicesControlServiceFabric.h"
#import "FGAFavouritesTableViewCellConfiguratorFabric.h"


@implementation FGAFavouritesTableViewControllerConfigurator

- (void) awakeFromNib {
  [super awakeFromNib];

  FGAFetchedResultControllerFabric *fetchedResultControllerFabric = [FGAFetchedResultControllerFabric new];
  NSFetchedResultsController *fetchedResultController =[fetchedResultControllerFabric favouritesFetchedResultController];
  self.favouritesTableViewController.fetchedResultController = fetchedResultController;
  fetchedResultController.delegate = self.favouritesTableViewController;

  FGAFavouriteAdvicesControlServiceFabric *favouriteAdvicesControlServiceFabric = [FGAFavouriteAdvicesControlServiceFabric new];
  id<FGAFavouriteAdvicesControlService> favouriteAdvicesControlService = [favouriteAdvicesControlServiceFabric favoriteAdvicesControlService];
  self.favouritesTableViewController.favouriteAdvicesControlService = favouriteAdvicesControlService;

  FGAFavouritesTableViewCellConfiguratorFabric *favouritesTableViewCellConfiguatorFabric = [FGAFavouritesTableViewCellConfiguratorFabric new];
  id<FGAFavouritesTableViewCellConfigurator> favouritesTableViewCellConfigurator = [favouritesTableViewCellConfiguatorFabric cellConfigurator];
  self.favouritesTableViewController.cellConfigurator = favouritesTableViewCellConfigurator;
}

@end
