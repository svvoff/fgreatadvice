//
//  FGAFavouritesTableViewControllerConfigurator.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FGAFavouritesTableViewController.h"

@interface FGAFavouritesTableViewControllerConfigurator : NSObject

@property (nonatomic, weak) IBOutlet FGAFavouritesTableViewController *favouritesTableViewController;

@end
