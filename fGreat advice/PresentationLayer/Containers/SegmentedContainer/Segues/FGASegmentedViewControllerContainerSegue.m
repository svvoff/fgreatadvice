//
//  SegmentedViewControllerContainerSegue.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/14/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGASegmentedViewControllerContainerSegue.h"
#import "FGASegmentedViewControllerContainer.h"

@implementation FGASegmentedViewControllerContainerSegue

- (void) perform {

  id<FGASegmentedViewControllerContainer> container = self.sourceViewController;

  if (![container conformsToProtocol:@protocol(FGASegmentedViewControllerContainer)]) {
    [NSException raise:@"IncorrectSourceViewController"
                format:@"Source View Controller should conform to %@ protocol",
     NSStringFromProtocol(@protocol(FGASegmentedViewControllerContainer))];
  }

  if (container.currentViewController) {
    [container toggleFromViewController:container.currentViewController toViewController:self.destinationViewController];
  } else {
    [container displayContentController:self.destinationViewController];
  }
}

@end
