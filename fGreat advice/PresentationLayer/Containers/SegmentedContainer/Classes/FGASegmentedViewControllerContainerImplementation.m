//
//  SegmentedViewControllerContainer.m
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/14/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import "FGASegmentedViewControllerContainerImplementation.h"
#import "FGASegmentedViewControllerContainerChild.h"


static NSString *const kSegmentedViewControllerContainerRootViewController = @"kSegmentedViewControllerContainerRootViewController";
static NSString *const kSegmentedViewControllerContainerSecondViewController = @"kSegmentedViewControllerContainerSecondViewController";
static NSTimeInterval const kSegmentedViewControllerContainerTransitionAnimationDuration = 0.3;

@interface FGASegmentedViewControllerContainerImplementation ()

@property (nonatomic, weak) IBOutlet UISegmentedControl *screenSegmentedControl;

@end

@implementation FGASegmentedViewControllerContainerImplementation

@synthesize currentViewController = _currentViewController;
@dynamic navigationItem;

- (void)viewDidLoad {
  [super viewDidLoad];
  [self.screenSegmentedControl addTarget:self
                                  action:@selector(segmentedControlDidChanged:)
                        forControlEvents:UIControlEventValueChanged];
  [self performSegueWithIdentifier:kSegmentedViewControllerContainerRootViewController sender:nil];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}


#pragma mark - UISegmentedControl

- (void) segmentedControlDidChanged: (UISegmentedControl *) segmentedControl {
  switch (segmentedControl.selectedSegmentIndex) {
    case 0:
      if (self.childViewControllers.count > 0) {
        [self toggleFromViewController:self.currentViewController toViewController:self.childViewControllers[0]];
      } else {
        [self performSegueWithIdentifier:kSegmentedViewControllerContainerRootViewController sender:nil];
      }
      break;

    case 1:
      if (self.childViewControllers.count > 1) {
        [self toggleFromViewController:self.currentViewController toViewController:self.childViewControllers[1]];
      } else {
        [self performSegueWithIdentifier:kSegmentedViewControllerContainerSecondViewController sender:nil];
      }
      break;

    default:
      break;
  }
}


#pragma mark - Container

- (CGRect) frameForContentController {
  return self.view.frame;
}

- (void) updateScrollViewInsetsOfViewController: (UIViewController *) viewController {
  if (viewController.automaticallyAdjustsScrollViewInsets && [viewController isKindOfClass:[UITableViewController class]]) {
    UITableView *tableView = [((UITableViewController *) viewController) tableView];
    tableView.contentInset = UIEdgeInsetsMake(self.topLayoutGuide.length, 0, 0, 0);
  }
}


#pragma mark - SegmentedViewControllerContainer

- (void) prepareViewController: (UIViewController *) content {
  if (![self.childViewControllers containsObject:content]) {
    [self addChildViewController:content];
  }
  self.currentViewController = content;
  content.view.frame = [self frameForContentController];
  [self updateScrollViewInsetsOfViewController:content];
  [self prepareNavigationItem];
  if ([content respondsToSelector:@selector(configureSegmentedViewControllerContainerNavigationItem:)]) {
    id child = content;
    [child configureSegmentedViewControllerContainerNavigationItem:self.navigationItem];
    [child setSegmentedContainer:self];
  }
}

- (void) prepareNavigationItem {
  self.navigationItem.rightBarButtonItems = nil;
  self.navigationItem.leftBarButtonItems = nil;
}

- (void) displayContentController: (UIViewController*) content {
  [self prepareViewController:content];
  [self.view addSubview:content.view];
  [content didMoveToParentViewController:self];
}

- (void) toggleFromViewController: (UIViewController *) oldViewController toViewController: (UIViewController *) newViewController {

  [self prepareViewController:newViewController];

  [self transitionFromViewController:oldViewController
                    toViewController:newViewController
                            duration:kSegmentedViewControllerContainerTransitionAnimationDuration
                             options:UIViewAnimationOptionTransitionCrossDissolve
                          animations:nil
                          completion:^(BOOL finished) {
                            [newViewController didMoveToParentViewController:self];
                          }];
}


@end
