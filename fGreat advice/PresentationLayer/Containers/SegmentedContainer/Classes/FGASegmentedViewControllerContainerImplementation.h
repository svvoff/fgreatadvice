//
//  SegmentedViewControllerContainer.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/14/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FGASegmentedViewControllerContainer.h"

@interface FGASegmentedViewControllerContainerImplementation: UIViewController <FGASegmentedViewControllerContainer>

@end
