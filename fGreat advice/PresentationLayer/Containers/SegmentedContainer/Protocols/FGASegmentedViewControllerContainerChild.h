//
//  FGASegmentedViewControllerContainerChild.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/15/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FGASegmentedViewControllerContainer;

@protocol FGASegmentedViewControllerContainerChild <NSObject>
@property (nonatomic, strong) id<FGASegmentedViewControllerContainer> segmentedContainer;

@optional
- (void) configureSegmentedViewControllerContainerNavigationItem: (UINavigationItem *) navigationItem;

@end
