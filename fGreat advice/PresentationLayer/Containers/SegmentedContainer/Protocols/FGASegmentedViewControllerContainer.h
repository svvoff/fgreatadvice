//
//  SegmentedViewControllerContainer.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/14/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FGASegmentedViewControllerContainer <NSObject>

@property (nonatomic, weak) UIViewController *currentViewController;
@property(nonatomic,readonly,strong) UINavigationItem *navigationItem;

- (void) displayContentController: (UIViewController*) content;
- (void) toggleFromViewController: (UIViewController *) oldViewController toViewController: (UIViewController *) newViewController;

@end
