//
//  FGAAdvice.h
//  fGreat advice
//
//  Created by Andrei Sorokin on 6/16/17.
//  Copyright © 2017 Andrei Sorokin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FGAAdvice : NSObject

@property (nonatomic, strong) NSNumber *idNumber;
@property (nonatomic, copy) NSString *text;

@end
